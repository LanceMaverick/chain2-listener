#!/usr/bin/env python
import os
import logging
from datetime import datetime
import websocket
import _thread
import time
import rel
import dataset
import json

DB_URL = os.environ['DB_URL']

logger = logging.getLogger()
logging.basicConfig(level=logging.INFO)

def on_message(ws, message):

    try:
        # Separate info events from energy data events
        json_packet = json.loads(message)
        if 'Chain2Info' in json_packet:
            table = 'Chain2Info'
        elif 'Chain2Data' in json_packet and json_packet['Chain2Data']:
            table = 'Chain2Data'
        else:
            raise Exception('Unknown packet type: {}'.format(message))
        
        measurement = json_packet[table]
        logger.info("New {} event".format(table))

        # Parse event timestamp
        measurement_datetime = datetime.strptime(measurement['Ts'], "%Y/%m/%d %H:%M:%S")
        measurement['Ts'] = int(measurement_datetime.timestamp())
        measurement['Ts2'] = measurement_datetime
        # Rename Id key to not conflict with db column  
        measurement["device_id"] = measurement.pop("Id")

        if table == 'Chain2Data':
            # flatten data structure of Chain2Data
            payload = measurement.pop('Payload')
            measurement.update(payload)
        else:
            # extract and flatten data structure of SNR measurements for info events
            lastSNR = measurement.pop("LastSNR")
            try:
                measurement['LastSNR_M1_Signal'] = lastSNR[0][0]
                measurement['LastSNR_M1_Noise'] = lastSNR[0][1]
            except Exception as e:
                logger.error("unable to parse SNR measurements: {}".format(lastSNR))
            avSNR = measurement.pop("AverageSNR")
            try:
                measurement['avSNR_M1_Signal'] = avSNR[0][0]
                measurement['avSNR_Noise'] = avSNR[0][1]
            except Exception as e:
                logger.error("unable to parse SNR measurements: {}".format(lastSNR))
        
        db = dataset.connect(DB_URL)
        logger.info("Writing event: {}".format(measurement))
        db[table].insert(measurement)
        db.close()

    except Exception as e:
        logging.error(e)
    else:
        logging.info("Written measurement: {}".format(message))

def on_error(ws, error):
    logger.error(error)

def on_close(ws, close_status_code, close_msg):
    logger.info("Websocket connection closed")

def on_open(ws):
    logger.info("Websocket connection established")

if __name__ == "__main__":
    #websocket.enableTrace(True)
    logger.info("Running...")
    ws = websocket.WebSocketApp("ws://192.168.1.3:81",
                              on_open=on_open,
                              on_message=on_message,
                              on_error=on_error,
                              on_close=on_close)

    ws.run_forever(dispatcher=rel, reconnect=5)  # Set dispatcher to automatic reconnection, 5 second reconnect delay if connection closed unexpectedly
    rel.signal(2, rel.abort)  # Keyboard Interrupt
    rel.dispatch()
